package client

import (
	"context"
	"errors"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"time"
)

const ApiKeyName = "apiKey"

type Client struct {
	Conn *grpc.ClientConn
}

type Auth struct {
	AppKey    string
	AppSecret string
	Metadata  map[string][]string
}

func (a *Auth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{"app_key": a.AppKey, "app_secret": a.AppSecret}, nil
}

func (a *Auth) RequireTransportSecurity() bool {
	return true
}

func NewClient(address, bid, privateKey string) (*Client, error) {
	if address == "" || bid == "" || privateKey == "" {
		return nil, errors.New("缺少必要参数")
	}
	auth := &Auth{
		AppKey:    bid,
		AppSecret: privateKey,
		Metadata: metadata.New(map[string]string{
			"authorization": fmt.Sprintf("Bearer %s %s", bid, privateKey),
		}),
	}
	conn, err := connect(address, auth)
	if err != nil {
		return nil, err
	}
	return &Client{
		Conn: conn,
	}, nil
}

func connect(address string, auth *Auth) (*grpc.ClientConn, error) {
	ctx, cel := context.WithTimeout(context.Background(), time.Second*10)
	defer cel()

	conn, err := grpc.DialContext(ctx, address, grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(auth.clientAuthInterceptor()),
	)

	if err != nil {
		return nil, err
	}
	return conn, nil
}

func (c *Client) Close() {
	if c.Conn != nil {
		c.Conn.Close()
	}
}

func (a *Auth) clientAuthInterceptor() grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		ctx = metadata.NewOutgoingContext(ctx, a.Metadata)
		err := invoker(ctx, method, req, reply, cc, opts...)
		return err
	}
}
