# 使用指南



## 如何在新的项目中引用SDK

```azure
go get gitlab.com/air-public/air-core-sdk-go
```



## 如何通过Client调用API服务

```azure
ctx, cel := context.WithTimeout(context.Background(), time.Second*10)
defer cel()
address := "CURRENT_SERVER_ADDRESS" //AIR Core服务的访问地址
bid := "YOUR_BID" //当前用户的BID
privateKey := "YOUR_PRIVATE_KEY" //当前用户的私钥
//初始化客户端
client, err := client.NewClient(address, bid, privateKey)
if err != nil {
	panic(err)
}
//声明某个业务接口
businessCategoryManager := api.NewBusinessCategoryManagerClient(client.Conn)
//调用业务接口的方法
result, err := businessCategoryManager.List(ctx, &api.GeneralRequest{})
if err != nil {
	panic(err)
}
//检查调用是否成功
if result.Result.Code != 200 {
	fmt.Println("Strange things")
}
```